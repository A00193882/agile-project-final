package testCases;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import daoClasses.DBController;
import junit.framework.TestCase;
import productCode.Customer;
import productCode.Employee;

public class EmployeeTest extends TestCase {

	// Test No:1
	// Objective:Test customer list in order by location then street number
	// Inputs:List<Customer>
	// Expected output: res = true
	public void testEmployeeOptimalSort001() throws Exception {
		Employee emp = new Employee();
		DBController db = new DBController();
		List<Customer> list = new ArrayList<Customer>();
		list = emp.optimalRouteSort();
		int north = db.countNorth();
		int south = db.countSouth();
		int east = db.countEast();
		int west = db.countWest();
		boolean res = true;
				for (int i = 0; i < north; i++) {
					if (i + 1 < north) {
						if (list.get(i).getStreetNo() > list.get(i+1).getStreetNo()) {
							res = false;
						}
					} else {
						i = north;
					}
				}
				for (int i = north+1; i < south; i++) {
					if (i + 1 < south) {
						if (list.get(i).getStreetNo() > list.get(i+1).getStreetNo()) {
							res = false;
						}
					} else {
						i = south;
					}
				}
				for (int i = north+south+1; i < east; i++) {
					if (i + 1 < east) {
						if (list.get(i).getStreetNo() > list.get(i+1).getStreetNo()) {
							res = false;
						}
					} else {
						i = east;
					}
				}
				for (int i = north+south+east+1; i < west; i++) {
					if (i + 1 < west) {
						if (list.get(i).getStreetNo() > list.get(i+1).getStreetNo()) {
							res = false;
						}
					} else {
						i = west;
					}
				}
		assertSame(true, res);

	}

	// Test No:2
	// Objective:Test customer list is not empty
	// Inputs:List<Customer>
	// Expected output:true
	public void testEmployeeOptimalSort002() throws Exception {
		Employee emp = new Employee();
		List<Customer> list = new ArrayList<Customer>();
		list = emp.optimalRouteSort();
		if (list.size() == 0) {
			fail("List has no entrys");
		} else {
			assertTrue(true);
		}
	}

	// Test No:3
	// Objective:verify customer region is either north south east or west
	// Inputs:List<Customer>
	// Expected output:true
	public void testEmployeeOptimalSort003() throws Exception {
		Employee emp = new Employee();
		List<Customer> list = new ArrayList<Customer>();
		list = emp.optimalRouteSort();
		for (int i = 0; i < list.size(); i++) {
			int region = list.get(i).getRegion();
			if ((region == 1) || (region == 2) || (region == 3) || (region == 4)) {
				assertTrue(true);
			} else {
				fail("Should not reach this point");
			}
		}

	}

	// Test No:4
	// Objective:verify customer address is not blank
	// Inputs:List<Customer>
	// Expected output:true
	public void testEmployeeOptimalSort004() throws Exception {
		Employee emp = new Employee();
		List<Customer> list = new ArrayList<Customer>();
		list = emp.optimalRouteSort();
		for (int i = 0; i < list.size(); i++) {
			String address = list.get(i).getAddress();
			if (address != "") {
				assertTrue(true);
			} else {
				fail("Should not reach this point");
			}

		}

	}
	
	Employee employeelogin = new Employee();

	// Test Login
		// No: 1
		// Test login method when both login and passwords are correct
		// Inputs: "A123", "B12344567"
		// Output: True

	public void testLogin001(int userId, String password) {
		System.out.println("Log 1");
		assertEquals(true, Employee.EmployeeLogin("A123", "B12344567"));
	}
	
	// Test Login
		// No: 2
		// Test login method when Id is wrong and passwords is right
		// Inputs: "A132", "B1234"
		// Output: False

		public void testLogin002() {
				System.out.println("Log 2");
				assertEquals(true, Employee.EmployeeLogin("A132", "B12344567"));

			}
		
		// Test Login
			// No: 3
			// Test login method when Id is right and passwords is wrong
			// Inputs: "A123", "B02344567"
			// Output: False

			public void testLogin003() {
					System.out.println("Log 3");
					assertEquals(true, Employee.EmployeeLogin("A123", "B02344567"));

				}
}
