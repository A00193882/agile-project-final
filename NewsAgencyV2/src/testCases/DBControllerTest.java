package testCases;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import daoClasses.DBController;
import junit.framework.TestCase;
import productCode.Customer;

public class DBControllerTest extends TestCase {

	// Test no.: 1
	// Test Objective: Test database connection
	// Input(s): none
	// Expected Outputs: connected to database successfully (boolean true)
	public void testDBControllerConnection() {
		// Create DB object
		DBController db = new DBController();
		// Test the method
		assertTrue(db.initiate_db_conn());
	}

	// Test no.: 3
	// Test Objective: Test Customer list is not empty
	// Input(s): none
	// Expected Outputs: connected to database successfully (boolean true)
	public void testListNotEmpty() throws SQLException{
		List<Customer> list = new ArrayList<Customer>();
		DBController db = new DBController();
		
		list = db.getCustomerList();
		
		if(list.size()==0){
			fail("Shoud not reach this point");
		}
		else{
			assertTrue(true);
		}
		
		
	}

}
