package testCases;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

import junit.framework.TestCase;
import productCode.Order;

public class DeliveryCostTest extends TestCase {
	
	DeliveryCostTest ConfigDelivery = new DeliveryCostTest();
	
	private static Object ConfigDelivery(String string, String string2) {
		// TODO Auto-generated method stub
		return null;
	}
	
	// Test Assigning Delivery Cost
	// No: 1
	// Test assigning method when both delivery ID and delivery cost are correct
	// Inputs: "1", ".50"
	// Output: True

public void testAssiging001(int deliveryid, int deliverycost) {
	System.out.println("Assigning 1");
	assertEquals(true, DeliveryCostTest.ConfigDelivery("1", ".50"));
}


// Test Assigning Delivery Cost
		// No: 1
		// Test assigning method where delivery ID is correct and delivery cost is not 
		// Inputs: "1", "1.50"
		// Output: false


	public void testAssiging002() {
			System.out.println("Assigning 2");
			assertEquals(true, DeliveryCostTest.ConfigDelivery("A132", "B12344567"));

		}
	
	// Test Assigning Delivery Cost
			// No: 1
			// Test assigning method where delivery ID is not correct and delivery cost is 
			// Inputs: "11", ".50"
			// Output: True

		public void testAssiging003() {
				System.out.println("Assigning 3");
				assertEquals(true, DeliveryCostTest.ConfigDelivery("A123", "B02344567"));

			}
		//*************************************************************************************************
		// Test Change Delivery Cost
		// No: 1
		// Test entering delivery cost more than 1.59
		// Inputs: "2"
		// Output: false
		
public void testChangeDeliveryCost() {
		//throws Exception {
	Order o = new Order(50);
	try {
		assertFalse(o.changeDeliveryCost(2.0f));
	} catch (Exception e) {
		e.printStackTrace();
	}
}

}
