package productCode;

public class LocationException extends RuntimeException {
	private static final long serialVersionUID = 1L;
 
    public LocationException() {
    	super("Location out of bounds error. Should be 1 - 4.");      
    }    
}


