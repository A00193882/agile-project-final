package productCode;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import productCode.User;

import daoClasses.DBController;


public class Employee {

	public List<Customer> optimalRouteSort() throws EmployeeExceptionHandler {
		DBController db = new DBController();
		List<Customer> list = new ArrayList<Customer>(10);
		try {
			list = db.getCustomerList();
		} catch (SQLException e) {
			e.printStackTrace();
		}

		return list;

	}
	 public boolean login(String username,String password)throws EmployeeExceptionHandler{

	       if(username==" "&&password==" "){
	    	   System.out.println("username or password can not be empty");
	       }
		      if(password.length()>7){
		       return true; }
		      
		      else if (Character.isUpperCase(password.charAt(0)) == true)	{
		    	  
		      return true;
		      }
		      else
	return false ;
   
		//return false;
	 }

	

	public boolean InsertCustomerdata(int cust_id,String firstname,String lastname,int street_num,String address,String  contactNo,int loc_id,int emp_id)throws EmployeeExceptionHandler, SQLException{
		DBController database = new DBController();
		 try {
			database.initiate_db_conn();
		} catch (Exception e) {
			e.printStackTrace();
		}

		 if(cust_id>0){	    	
		// if (firstname.length()>0||lastname.length()>0||address.length()>0||street_num>0||contactNo.length()>0||loc_id>0||emp_id>0){
		 if (database.InsertCustomerdata(cust_id,firstname,lastname,street_num,address,contactNo,loc_id,emp_id)){
			 System.out.println("Insert successful!");
			 return true;
		 }
	 else
	     throw new EmployeeExceptionHandler("every detail can not be empty");
		  
	 }
	
	 else
		 throw new EmployeeExceptionHandler("cust_id can not be empty!!");

	 }
	 public boolean DeleteCustomerdata(int cust_id)throws EmployeeExceptionHandler{
		 DBController database = new DBController();
			
		 try {
			database.initiate_db_conn();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		     if(cust_id>0){
				 System.out.println("delete by cust_id:" +cust_id);
		    	 return true;
			 }
			
			 else
				 
				 throw new EmployeeExceptionHandler("you must to input cust_id");
		 }

	
	
		
	
		
		

	/*
	 * public static void main(String args[]) throws EmployeeExceptionHandler {
	 * List<Customer> list = new ArrayList<Customer>(10); list =
	 * optimalRouteSort(); for (int i = 0; i < list.size(); i++) {
	 * System.out.println(list.get(i).getCustomerId() + "\n" +
	 * list.get(i).getFirstname() + "\n" + list.get(i).getLastname()+ "\n" +
	 * list.get(i).getRegion() + "\n" + list.get(i).getStreetNo() + "\n" +
	 * list.get(i).getAddress() + "\n"); }
	 * 
	 * }
	 */

	DBController database = new DBController();
	User user = new User(null, null);

	public String get_information() {
		return user.getUserId() + "" + user.getPassword();
	}

	public boolean Employeelogin(String username, String password) throws EmployeeExceptionHandler {

		try {
			database.initiate_db_conn();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (username != "" && password != "") {
			if (password.length() >= 7) {
				if (Character.isUpperCase(password.charAt(0)) == true) {
					if (database.Employeelogin(username, password)) {
						return true;
					} else
						throw new EmployeeExceptionHandler("Invalid username or password");
				} else
					throw new EmployeeExceptionHandler("Invalid  password, no upper case");
			} else
				throw new EmployeeExceptionHandler("Invalid  password,less than 7 digit");
		} else
			throw new EmployeeExceptionHandler("Username and password can't be blank");

	}

	public boolean DayList() throws EmployeeExceptionHandler {
		try {
			database.initiate_db_conn();
		} catch (Exception e) {
			e.printStackTrace();
		}

		List<DayOfWeek> day = database.list();
		if (day.size() > 0) {
			System.out.println("dayID  dayName");
			for (int i = 0; i < day.size(); i++) {
				System.out.println(day.get(i).getDayId() + "    " + day.get(i).getDayName());
			}
			return true;
		} else
			throw new EmployeeExceptionHandler("There is no day can be choosen!");
	}

	public boolean publicationList() throws EmployeeExceptionHandler {

		// This is productCode for list publication table code
		List<Publication> publication = database.list1();
		try {
			database.initiate_db_conn();
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (publication.size() > 0) {
			System.out.println("ID  Name  Cost  ");
			for (int i = 0; i < publication.size(); i++) {

				System.out.println(publication.get(i).getPublicationId() + "  " + publication.get(i).getName() + "  "
						+ publication.get(i).getCost() + "  ");
			}
			return true;
		} else
			throw new EmployeeExceptionHandler("invalid");
	}

	// This is productCode check login code
	public boolean checklogin(String name, String pwd) throws EmployeeExceptionHandler {
		try {
			database.initiate_db_conn();
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (name != "" && pwd != "") {
			if (pwd.length() >= 7) {
				if (Character.isUpperCase(pwd.charAt(0)) == true) {
					if (database.checklogin(name, pwd)) {
						return true;
					}

					else
						throw new EmployeeExceptionHandler("Invalid username or password");
				} else
					throw new EmployeeExceptionHandler("Invalid password");
			} else
				throw new EmployeeExceptionHandler("Invalid password");
		} else
			throw new EmployeeExceptionHandler("Username and password can't be blank");
	}
	
	
	public boolean UpdatePaymentsStatus(int order_id, String paymemntStatus, String deliveryStatus)
			throws EmployeeExceptionHandler, SQLException {
		Order order = new Order();
		order.setOrderId(order_id);
		order.setPaymentStatus(paymemntStatus);
		order.setDeliveryStatus(deliveryStatus);
		String pay_Sta = order.getPaymentStatus();
		String deli_Sta = order.getDeliveryStatus();

		if (order_id > 0 && paymemntStatus != ""&&deliveryStatus != "") {
			try {
				database.updatepaymentStatus(order.getOrderId(), pay_Sta, deli_Sta);
				return true;

			} catch (SQLException e) {
				// TODO Auto-generated catch block

				throw new EmployeeExceptionHandler("you input the wrong value");
			}

		}

		else
			System.out.println("no value found");
		   throw new EmployeeExceptionHandler("do not leave the blank area");

	}
	
	public boolean UpdateDeliveryStatus(int order_id, String paymemntStatus, String deliveryStatus)
			throws EmployeeExceptionHandler {
		Order order = new Order();
		order.setOrderId(order_id);
		order.setPaymentStatus(paymemntStatus);
		order.setDeliveryStatus(deliveryStatus);
		String pay_Sta = order.getPaymentStatus();
		String deli_Sta = order.getDeliveryStatus();

		if (order_id > 0 && paymemntStatus != ""&&deliveryStatus != "") {
			try {
				database.updateDeliveryStatus(order.getOrderId(), pay_Sta, deli_Sta);
				return true;

			} catch (SQLException e) {
				// TODO Auto-generated catch block

				throw new EmployeeExceptionHandler("you input the wrong value");
			}

		}

		else
			System.out.println("no value found");
		throw new EmployeeExceptionHandler("do not leave the blank area");

	}
	
	
	
	
	
	
	
	
	
	
	
	
	

	public boolean verifypaymentStatus(int orderID) throws EmployeeExceptionHandler, SQLException {

		// try {
		// database.connect();
		// } catch (DbExceptionHandler e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }

		Order first = new Order();
		first.setOrderId(orderID);
		first.setPaymentStatus(database.checkPaymentStatus(first.getOrderId()));
		if (first.getPaymentStatus().equals("paid") || first.getPaymentStatus().equals("unpaid")) {
			System.out.println("the value is " + database.checkPaymentStatus(first.getOrderId()));
			return true;
		} else {
			System.out.println("the value is " + database.checkPaymentStatus(first.getOrderId()));
			throw new EmployeeExceptionHandler("it is the wrong value");
		}

	}
	
	public boolean verifydileveryStatus(int orderID) throws EmployeeExceptionHandler, SQLException {
		Order first = new Order();
		first.setOrderId(orderID);
		first.setDeliveryStatus(database.checkdeliveryStatus(first.getOrderId()));

		if (first.getDeliveryStatus().equals("frozen") || first.getDeliveryStatus().equals("granted")) {
			System.out.println("the value is " + database.checkdeliveryStatus(first.getOrderId()));

			return true;

		} else {
			System.out.println("the value is " + database.checkdeliveryStatus(first.getOrderId()));
			throw new EmployeeExceptionHandler("it is  the wrong value");
		}
	}
	
	public static boolean EmployeeLogin(String userName, String Pword) {
		DBController db = new DBController();
		db.initiate_db_conn();
		try {
			String updateLogin = "select * from login where userName like '" + userName + "';";
			ResultSet rs = db.stmt.executeQuery(updateLogin);
			rs.next();

			if (Pword.equals(rs.getString("Pword"))) {
				@SuppressWarnings("unused")
				User user = new User(userName, Pword);
				return true;

			} else
				System.out.println("Wrong Password or login has been inputed");
			rs.close();
			return false;

		} catch (SQLException sqle) {
			System.err.println("Error:\n Wrong Password or login has been inputed");
			System.err.println("Error with  insert:\n" + sqle.toString());
			return false;
		}
	}
}
	
	
	
	
	
	
	
	
	
	

