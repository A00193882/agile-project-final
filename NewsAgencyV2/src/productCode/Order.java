package productCode;

import java.sql.SQLException;

import daoClasses.DBController;

public class Order {

	private int orderId;
	private int quantity;
	private Publication publicationId;
	private Customer customer;
	private int dayId;
	private int deliveryConfigurationId;
	private String paymentStatus;
	private String deliveryType;
	private String deliveryStatus;
	private String deliverySuccess = "Successful";
	private String deliveryUnsuccessful = "Unsuccessful";
	private DBController db;
	private DBController database= new DBController();

	
	public Order(){
		
	}
	// Constructor for checking orders in the DB using order ID reference
	public Order(int orderId) {
		this.orderId = orderId;
	}

	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getPublicationId() {
		return publicationId.getPublicationId();
	}

	public int getCustomerId() {
		return customer.getCustomerId();
	}

	public int getDayId() {
		return dayId;
	}

	public void setDayId(int dayId) {
		this.dayId = dayId;
	}
	public String getDeliveryStatus() {
		return deliveryStatus;
	}
	public void setDeliveryStatus(String deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}
	public int getLocationId() {
		return getLocationId();
	}

	public int getDeliveryConfiguration() {
		return deliveryConfigurationId;
	}

	public void setDeliveryConfiguration(int deliveryConfiguration) {
		this.deliveryConfigurationId = deliveryConfiguration;
	}

	public String getPaymentStatus() {
		return paymentStatus;
	}

	public void setPaymentStatus(String paymentStatus) {
		this.paymentStatus = paymentStatus;
	}

	public String getAccountStatus() {
		return deliveryType;
	}

	public void setAccountStatus(String accountStatus) {
		this.deliveryType = accountStatus;
	}

	public String getDeliveryStatus(int orderId) {
		db.initiate_db_conn();
		return db.checkDbDeliverySuccess(orderId);
	}

	// Method to set the delivery status for a specific order
	// to "Successful"
	public boolean setDeliveryStatusSuccessful(int id) {
		db.initiate_db_conn();
		this.deliveryStatus = deliverySuccess;
		if (db.setDeliveryStatus(id, deliverySuccess))
			return true;
		else
			return false;
	}

	// Method to set the delivery status for a specific order
	// to "Unsuccessful"
	public boolean setDeliveryStatusUnsuccessful(int id) {
		db.initiate_db_conn();
		this.deliveryStatus = deliveryUnsuccessful;
		if (db.setDeliveryStatus(id, deliveryUnsuccessful))
			return true;
		else
			return false;
	}

	public boolean setDeliveryType(int cus_id) throws SQLException {

		int id = cus_id;

		String DelType = getDeliveryType(id);

		if (DelType.equals("frozen")) {
			try {
				// call db controller to change delivery type granted
				db.delivery_g_Update(cus_id);
				// Print to verify change have taken place
				System.out.println("Now changed to Granted");
			} catch (Exception e) {
				System.out.println("Error");
			}
		} else if (DelType.equals("granted")) {
			try {
				// call db controller to change delivery type frozen
				db.delivery_f_Update(cus_id);
				// Print to verify change have taken place
				System.out.println("Now changed to Frozen");

			} catch (Exception e) {
				System.out.println("Error");
			}
		}
		
		return true;

	}

	public String getDeliveryType(int id) throws SQLException {
		// check current customer delivery type
		return db.delivery_type(id);
	}
	
	   public boolean DeliveryStatus(int cust_id)throws EmployeeExceptionHandler{	

			 try {
					database.initiate_db_conn();
				} catch (Exception e) {
					e.printStackTrace();
				}
	    	
	    	if(database.getDeliveryStatus(cust_id)){
	    	  return true;
	    	}
	    	else throw new EmployeeExceptionHandler("invalid");

	    }
	    
	    public boolean keepRecord(String status)throws EmployeeExceptionHandler{

			 try {
					database.initiate_db_conn();
				} catch (Exception e) {
					e.printStackTrace();
				}
	    	if(database.keepRecord(status)){
	    		return true;
	    	}
	    	else throw new EmployeeExceptionHandler("invalid");
	    }
	    
	    
	  //This is productCode select newspaper code
		public boolean selectnewspaper(int id, String type)throws EmployeeExceptionHandler{
			
			try{
				 database.initiate_db_conn();
				}catch(Exception e){
					e.printStackTrace();
				}
				
				 if(database.selectnewspaper(id, type)){
					return true;
				 }
				 
			  else throw new EmployeeExceptionHandler("select newspaper Unsuccesssful");
			 }
		//This is productCode select magazine code
		public boolean selectmagzine(int id, String type)throws EmployeeExceptionHandler{
			
			try{
				 database.initiate_db_conn();
				}catch(Exception e){
					e.printStackTrace();
				}
				
				 if(database.selectmagazine(id, type)){
					return true;
				 }
				 
			  else throw new EmployeeExceptionHandler("select magazin Unsuccesssful");
			 }	
		
		
		public boolean changeDeliveryCost(float cost) throws SQLException {

			try {
				DBController db = new DBController();
				db.initiate_db_conn();
			} catch (Exception e) {

				e.printStackTrace();
			}
			int newCost = 0;
			if (cost < 1.50 && cost > 0) {
			
				newCost = db.changeDeliveryCost(cost);
				System.out.println(newCost + " the delivery cost have been modified ");
			} else
				System.out.println("Errror try again");
			if (newCost == 1) {
				System.out.println("csuccessful");
				return true;

			} else
				return false;
		}
}
