package productCode;

public class Delivery_Config {
	private int del_conf_id;
	private int delivey_cost;
	
public Delivery_Config(int delId,int cost2){
	super();
	del_conf_id=delId;
	this.delivey_cost = cost2;
}

public Delivery_Config(int cost2) {
	this.delivey_cost = cost2;
}

public int getDel_conf_id() {
	return del_conf_id;
}

public void setDel_conf_id(int del_conf_id) {
	this.del_conf_id = del_conf_id;
}

public int getDelivey_cost() {
	return delivey_cost;
}

public void setDelivey_cost(int delivey_cost) {
	this.delivey_cost = delivey_cost;
}

}
