package productCode;

import daoClasses.DBController;

public class Publication {
	
	private int publicationId;
	private String name;
	DBController database= new DBController();
	private int cost; 
	
	public Publication(int pubId, String pubName) {
		this.publicationId = pubId;
		this.name = pubName;
	}
	
	public Publication(int pub_id, String pubName, int cost) {
		this.publicationId = pub_id;
		this.name = pubName;
		this.cost = cost;
	}
	public int getCost() {
		return cost;
	}

	public void setCost(int cost) {
		this.cost = cost;
	}

	public Publication(int pubId) {
		this.publicationId = pubId;
	}
	
	
	public int getPublicationId() {
		return publicationId;
	}
	public void setPublicationId(int publicationId) {
		this.publicationId = publicationId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}


}
