package productCode;

@SuppressWarnings("serial")
public class EmployeeExceptionHandler extends Exception{

	String message;

	public EmployeeExceptionHandler(String errMessage){
		message = errMessage;
	}

	public String getMessage(){
		return message;
	}

}
