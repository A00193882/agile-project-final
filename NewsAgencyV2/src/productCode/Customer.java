package productCode;

import daoClasses.DBController;

public class Customer {

	private int CustomerId;
	private String firstname;
	private String lastname;
	private int streetNo;
	private String address;
	private String contactNo;
	private int region;
	
	private DBController db = new DBController();// Instance of DBController

	public Customer(int customerId, String firstname, String lastname,int streetNo, String address, String contactNo,int region) {
		super();
		CustomerId = customerId;
		this.firstname = firstname;
		this.lastname = lastname;
		this.streetNo = streetNo;
		this.address = address;
		this.contactNo = contactNo;
		this.region = region;
	}
	

	public int getStreetNo() {
		return streetNo;
	}


	public void setStreetNo(int streetNo) {
		this.streetNo = streetNo;
	}


	public String getContactNo() {
		return contactNo;
	}

	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}

	public int getRegion() {
		return region;
	}

	public void setRegion(int region) {
		this.region = region;
	}

	public int getCustomerId() {
		return CustomerId;
	}

	public void setCustomerId(int customerId) {
		CustomerId = customerId;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String toString() {
		return firstname + "";
	}
	
	// getLocation(int) takes a customer id (int) as an argument and returns
	// the location of that customer as is stored in the db
	public int getLocationFromDB(int id) {
		db.initiate_db_conn();
		return db.getCustomerLocation(id);
	}

	// setLocation method takes a customer location (int) as an argument 
	// and sets the location of that customer in the current customer object
	// Surround your usage of setLocation with try, catch to handle exception
	public boolean setLocation(int location) {
		// if location is between 1 and 4 inclusive
		if((location >= 1) && (location <= 4)){
			this.region = location;
			return true;
		}
		else
			throw new LocationException();// Throw exception
	}

}
