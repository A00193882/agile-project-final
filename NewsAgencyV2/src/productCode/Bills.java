package productCode;
import daoClasses.DBController;
import daoClasses.DbExceptionHandler;
public class Bills {
private int billId;	
private int orderId;
private int publicationId;
private int deliveryConfigurationId;
private int sum;	
private int quantity;
private Publication cost;
private Delivery_Config delivey_cost;
private DBController db;
DBController database= new DBController();

public Bills(int su,int qty,int cost,int cost2){
	this.sum=su;
	this.quantity = qty;
	this.cost = new Publication( cost);
	this.delivey_cost= new Delivery_Config(cost2);	
}

public Bills() {

}

public int getQuantity() {
	return quantity;
}

public void setQuantity(int quantity) {
	this.quantity = quantity;
}

public Publication getCost() {
	return cost;
}

public void setCost(Publication cost) {
	this.cost = cost;
}

public Delivery_Config getDelivey_cost() {
	return delivey_cost;
}

public void setDelivey_cost(Delivery_Config delivey_cost) {
	this.delivey_cost = delivey_cost;
}

public boolean CalculateBill(int bill_id,int ord_id, int quantity,int del_con_id,int delivery_cost,int pub_id,int cost,int sum) throws EmployeeExceptionHandler {
	 try {
			database.initiate_db_conn();
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		     if(bill_id>0){
			 if (database.CalculateBill( bill_id, ord_id,  quantity, del_con_id, delivery_cost, pub_id, cost, sum)){
				 System.out.println("The calculate bills id:" +bill_id + " "+"and the bill result is:"+ sum);
				 return true;
			 }
			
			 else
				 throw new EmployeeExceptionHandler("the result is false!");
		 }
		
		 
		 else 
				 throw new EmployeeExceptionHandler("you must be input bill_id");
		     }
		

public int getBillId() {
	return billId;
}

public void setBillId(int billId) {
	this.billId = billId;
}

public int getOrderId() {
	return orderId;
}

public void setOrderId(int orderId) {
	this.orderId = orderId;
}

public int getPublicationId() {
	return publicationId;
}

public void setPublicationId(int publicationId) {
	this.publicationId = publicationId;
}

public int getDeliveryConfigurationId() {
	return deliveryConfigurationId;
}

public void setDeliveryConfigurationId(int deliveryConfigurationId) {
	this.deliveryConfigurationId = deliveryConfigurationId;
}

public int getSum() {
	return sum;
}

public void setSum(int sum) {
	this.sum = sum;
}
	
	
}



