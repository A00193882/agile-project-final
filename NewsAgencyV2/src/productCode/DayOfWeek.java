package productCode;

import daoClasses.DBController;

public class DayOfWeek {
	
	private int dayId;
	private String dayName;
	DBController database= new DBController();
	
	public DayOfWeek(int dayID,String dayName){
		this.dayId=dayID;
		this.dayName=dayName;
	}
	
	public int getDayId() {
		return dayId;
	}
	public void setDayId(int dayId) {
		this.dayId = dayId;
	}
	public String getDayName() {
		return dayName;
	}
	public void setDayName(String dayName) {
		this.dayName = dayName;
	}
	
	//This is productCode select day code
		public boolean selectday(int id,String day)throws EmployeeExceptionHandler{
			
			try{
				 database.initiate_db_conn();
				}catch(Exception e){
					e.printStackTrace();
				}
						
				 if(database.selectday(id, day)){
					return true;
				 }
				 
				 else  throw new EmployeeExceptionHandler("Invalid day");
			 }
		
		
		//This is productCode select Sunday code
		public boolean selectsunday(int id,String day)throws EmployeeExceptionHandler{
			
			try{
				 database.initiate_db_conn();
				}catch(Exception e){
					e.printStackTrace();
				}
						
				 if(database.selectsunday(id, day)){
					return true;
				 }
				 
				 else  throw new EmployeeExceptionHandler("Invalid day");
			 }
		//This is productCode select Monday code
		public boolean selectmonday(int id,String day)throws EmployeeExceptionHandler{
			
			try{
				 database.initiate_db_conn();
				}catch(Exception e){
					e.printStackTrace();
				}
						
				 if(database.selectmonday(id, day)){
					return true;
				 }
				 
				 else  throw new EmployeeExceptionHandler("Invalid day");
			 }
		//This is productCode select Tuesday code
		public boolean selecttuesday(int id,String day)throws EmployeeExceptionHandler{
			
			try{
				 database.initiate_db_conn();
				}catch(Exception e){
					e.printStackTrace();
				}
						
				 if(database.selecttuesday(id, day)){
					return true;
				 }
				 
				 else  throw new EmployeeExceptionHandler("Invalid day");
			 }
		//This is productCode select Wednesday code
		public boolean selectwednesday(int id,String day)throws EmployeeExceptionHandler{
			
			try{
				 database.initiate_db_conn();
				}catch(Exception e){
					e.printStackTrace();
				}
						
				 if(database.selectwednesday(id, day)){
					return true;
				 }
				 
				 else  throw new EmployeeExceptionHandler("Invalid day");
			 }
		//This is productCode select Thursday code
		public boolean selectthursday(int id,String day)throws EmployeeExceptionHandler{
			
			try{
				 database.initiate_db_conn();
				}catch(Exception e){
					e.printStackTrace();
				}
						
				 if(database.selectthursday(id, day)){
					return true;
				 }
				 
				 else  throw new EmployeeExceptionHandler("Invalid day");
			 }
		//This is productCode select Friday code
		public boolean selectfriday(int id,String day)throws EmployeeExceptionHandler{
			
			try{
				 database.initiate_db_conn();
				}catch(Exception e){
					e.printStackTrace();
				}
						
				 if(database.selectfriday(id, day)){
					return true;
				 }
				 
				 else  throw new EmployeeExceptionHandler("Invalid day");
			 }
		//This is productCode select Saturday code
		public boolean selectsaturday(int id,String day)throws EmployeeExceptionHandler{
			
			try{
				 database.initiate_db_conn();
				}catch(Exception e){
					e.printStackTrace();
				}
						
				 if(database.selectsaturday(id, day)){
					return true;
				 }
				 
				 else  throw new EmployeeExceptionHandler("Invalid day");
			 }
	

}
