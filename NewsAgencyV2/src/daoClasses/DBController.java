package daoClasses;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import productCode.Customer;

import productCode.EmployeeExceptionHandler;

import productCode.DayOfWeek;

import productCode.Publication;


public class DBController {

	public Statement stmt = null;
	ResultSet rs = null;
	Connection con = null;

	public boolean initiate_db_conn() {
		try {
			// Load the JConnector Driver
			Class.forName("com.mysql.jdbc.Driver");
			// Specify the DB Name
			String url = "jdbc:mysql://localhost:3306/newsAgency";
			// Connect to DB using DB URL, Username and password
			con = DriverManager.getConnection(url, "root", "admin");
			// Create a generic statement which is passed to the
			// TestInternalFrame1
			stmt = con.createStatement();
			return true;
		} catch (Exception e) {
			System.out.println("Error: Failed to connect to database\n" + e.getMessage());
		}
		return false;
	}

	public List<Customer> getCustomerList() throws SQLException {
		List<Customer> custList = new ArrayList<Customer>(5);
		initiate_db_conn();
		PreparedStatement psmt = con.prepareStatement("Select * from customers order by Loc_id,Address,street_num");
		ResultSet rs = psmt.executeQuery();
		while (rs.next()) {
			Customer cust = new Customer(rs.getInt("cust_id"), rs.getString("firstname"), rs.getString("lastname"),
					rs.getInt("street_num"), rs.getString("address"), rs.getString("contactNo"), rs.getInt("Loc_id"));
			custList.add(cust);
		}
		rs.close();
		psmt.close();
		con.close();
		return custList;

	}

	public int countNorth() throws SQLException {
		initiate_db_conn();
		int res = 0;
		String countNorth = "Select Count(*) From customers where Loc_id = 1";

		rs = stmt.executeQuery(countNorth);
		for (; rs.next();) {
			res = ((Number) rs.getObject(1)).intValue();
		}
		return res;

	}

	public int countSouth() throws SQLException {
		initiate_db_conn();
		int res = 0;
		String countSouth = "Select Count(*) From customers where Loc_id = 2";

		rs = stmt.executeQuery(countSouth);
		for (; rs.next();) {
			res = ((Number) rs.getObject(1)).intValue();
		}
		return res;

	}

	public int countEast() throws SQLException {
		initiate_db_conn();
		int res = 0;
		String countEast = "Select Count(*) From customers where Loc_id = 3";
		stmt.executeQuery(countEast);
		for (; rs.next();) {
			res = ((Number) rs.getObject(1)).intValue();
		}
		return res;
	}

	public int countWest() throws SQLException {
		initiate_db_conn();
		int res = 0;
		String countWest = "Select Count(*) From customers where Loc_id = 4";
		stmt.executeQuery(countWest);
		for (; rs.next();) {
			res = ((Number) rs.getObject(1)).intValue();
		}
		return res;
	}

	public String checkDbDeliverySuccess(int orderId) {
		// (READ) Execute the SQL statement "select" for orderId
		// passed into the method and get the result set
		String query = "SELECT * FROM orders WHERE ord_id = " + orderId + ";";
		try {
			rs = stmt.executeQuery(query);// execute query
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {// result set
			if (rs.next()) {
				if (rs.getString("delivery_status").equals("Successful"))

					return "Successful";// return string for success on selected
										// delivery

				else
					return "Unsuccessful";// return default
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return "Unsuccessful";
	}

	public boolean setDeliveryStatus(int id, String string) {

		// (Update) Execute the SQL statement "update" for delivery_status with
		// id and
		// status string passed into the method and return true if ok, false if
		// not
		if (string.equals("Successful") || string.equals("Unsuccessful")) {// input
																			// validation
			String update = "UPDATE newsagency.orders SET delivery_status = '" + string + "'WHERE ord_id = " + id + ";";
			try {
				int res = stmt.executeUpdate(update);// execute update
				if (res == 1)// we have updated something
					return true;
				else
					return false;

			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return false;
	}
	
	public boolean setupCustomer(Customer customer) {
		if(customer.getRegion() >= 1 && customer.getRegion() <= 4){// input validation for location
			// (insert) Execute the SQL statement "insert" for customer object passed to DAO
			// and get the results
			String insert = "INSERT into newsagency.customers VALUES (null, '" + customer.getFirstname() + "', '"
			+ customer.getLastname() + "', " + customer.getStreetNo() + ", '" + customer.getAddress() + "', '"
			+ customer.getContactNo() + "', " + customer.getRegion() + ");";
			try {
				System.out.println(insert);
				int res = stmt.executeUpdate(insert);// execute update
				if(res == 1)// we have inserted something
					return true;
				else 
					return false;						
			} catch (SQLException e) {
				e.printStackTrace();
			}	
		}
		return false;
	}

	public int getCustomerLocation(int id) {
		// (READ) Execute the SQL statement "select" for cust_id
		// passed into the method and get the result set
		String query = "SELECT loc_id FROM orders WHERE cust_id = " + id + ";";
		try {
			rs = stmt.executeQuery(query);// execute query
		} catch (SQLException e) {
			e.printStackTrace();
		}

		try {// result set
			if (rs.next()) {
				int location = rs.getInt("loc_id");
				return location;// return int for location
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return 0;// Return 0 if nothing in result set
	}


	 public boolean Login(String username,String PWord) throws SQLException{
	    		
				String updateTemp ="select * from employees "+"where userName= '"+username+"'"+"and pWord= '"+PWord+"'";
				
				rs=stmt.executeQuery(updateTemp);
				
				if(rs.next()){
					System.out.println(rs.getInt("emp_id"));
					return true;
				}
				else 
					return false;
	 }
	public String delivery_type(int custId) throws SQLException {

		String order_type = null;
		int cust_id = custId;

		String UpdateTemp = ("SELECT delivery_type FROM orders WHERE cust_id =" + cust_id);
		// stmt.setInt(0,cust_id);
		rs = stmt.executeQuery(UpdateTemp);

		if (rs.next()) {
			order_type = rs.getString("delivery_type");
			// System.out.println("Delivery Type:"+order);
		}

		return order_type;

	}

	public boolean delivery_f_Update(int custId) throws SQLException {

		int cust_id = custId;

		String UpdateTemp = ("UPDATE orders SET delivery_type = 'frozen' WHERE cust_id =" + cust_id);

		int Exrs = stmt.executeUpdate(UpdateTemp);

		// return row count for sql DML statement and nothing for no row change
		if (Exrs > 0) {
			return true;
		} else {
			return false;

		}

	}

	public boolean delivery_g_Update(int custId) throws SQLException {

		int cust_id = custId;

		String UpdateTemp = ("UPDATE orders SET delivery_type = 'granted' WHERE cust_id =" + cust_id);

		int Exrs = stmt.executeUpdate(UpdateTemp);
		System.out.println(+Exrs);

		// return row count for sql DML statement and nothing for no row change
		if (Exrs > 0) {
			return true;
		} else {
			return false;

		}

	}
	
	public boolean Employeelogin(String username, String pWord) {
		try
		{
			String updateTemp ="select * from employees "+"where userName= '"+username+"'"+"and pWord= '"+pWord+"'";			
			rs=stmt.executeQuery(updateTemp);			
			if(rs.next()){
				System.out.println("Login success!");
				return true;
			}
			else
			return false;

		}
		catch (SQLException sqle)
		{
			System.err.println("Error with login:\n"+sqle.toString());
			return false;
		}
	}
	
	 public List<DayOfWeek> list(){
	     List<DayOfWeek> DayList = new ArrayList<DayOfWeek>();
	     try {
	        String selectDayList="select * From daysofweek";
	         rs= stmt.executeQuery(selectDayList);
	        while (rs.next()) {

	        	DayOfWeek b = new DayOfWeek(rs.getInt("day_id"), rs.getString("dayOW"));
	        	DayList.add(b);

	        }
	     } catch (SQLException e) {
	        e.printStackTrace();
	     }
	     
	     return DayList;
	  }
	 
	 public boolean getDeliveryStatus(int CustomerID){
		 if(CustomerID>0){
		 try {
		       String select="SELECT delivery_status FROM orders where cust_id="+CustomerID+"";
		       rs= stmt.executeQuery(select);
		       
		       if(rs.next()) {
		    	  String s=rs.getString(CustomerID);
		    	  System.out.println("the delivery status is "+s);
                  return true;  
		       }
		       else{
		    	   System.out.println("no value found");
		    	   return false;
		       }
		    } catch (SQLException e) {
		    	System.out.println("no value found");
		    	return false;
		    }
		 }
		 else
			 System.out.println("no value found");
             return false;
		 }
	 
	 public boolean keepRecord(String status){
		 if(status!="")
		 try {
		       String Query="SELECT firstName FROM customers c,orders o where c.cust_id=o.cust_id and o.delivery_status='"+status+"'";
		       rs= stmt.executeQuery(Query);
	
		        while(rs.next()){
		    	  String s=rs.getString("firstName");
		    	  System.out.println("this status is belongs to "+s);
		    	  return true;
		    	  
		       }
		    	   System.out.println("no value found1");
		    	    return false;
		      
			 }  
			  catch (SQLException e) {
		    	System.out.println("no value found2");
		       
		    }
			  System.out.println("no value found3");
			    return false;
			    
		 }
	 // This is DBController checkLogin code 
	 public boolean checklogin(String name, String pwd) {
			try
			{
				String updateTemp ="select * from employees "+"where userName= '"+name+"'"+"and pWord= '"+pwd+"'";			
				rs=stmt.executeQuery(updateTemp);			
				if(rs.next()){
					System.out.println("Login success!");

					return true;
				}
				else
				return false;

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with login:\n"+sqle.toString());
				return false;
			}
		}

	    public boolean InsertCustomerdata(int cust_id,String firstname,String lastname,int street_num,String address,String contactNo,int loc_id,int emp_id)throws EmployeeExceptionHandler, SQLException{

				String updateTemp ="INSERT INTO customers VALUES("+
				null +",'"+firstname+"','"+lastname+"',"+street_num+",'"+address+"','"
				+contactNo+"',"+loc_id+","+emp_id+");";

				stmt.executeUpdate(updateTemp);
				System.out.println(updateTemp);	
				return true;

	    }
	//This is DBController choose newspaper and granted code
		public boolean selectnewspaper(int id, String type) {
			try
			{
				String updateTemp ="select * from orders "+"where cust_id= '"+id+"'"+"and delivery_type= '"+type+"'";			
				rs=stmt.executeQuery(updateTemp);			
				if(rs.next()){
					System.out.println("Select newspaper Success!");
					return true;
				}
				else
				return false;

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with select:\n"+sqle.toString());
				return false;
			}
		}
		//This is DBController choose magazine
		public boolean selectmagazine(int id, String type) {
			try
			{
				String updateTemp ="select * from orders "+"where cust_id= '"+id+"'"+"and delivery_type= '"+type+"'";			
				rs=stmt.executeQuery(updateTemp);			
				if(rs.next()){
					System.out.println("Select magazine Success!");
					return true;
				}
				else
				return false;


			}
			catch (SQLException sqle)
			{

				System.err.println("Error with  insert:\n"+sqle.toString());
			}
			return false;
	    	
	    }
	    public boolean DeleteCustomerdata(int cust_id)throws EmployeeExceptionHandler{
	    	try
			{
	    		String updateTemp ="DELETE FROM customers WHERE cust_id = "+cust_id+";"; 
				stmt.executeUpdate(updateTemp);
				System.err.println(updateTemp);	
				return true;
			}
			catch (SQLException sqle)
			{
				System.err.println("Error with  delete:\n"+sqle.toString());
			}
			return false;
	    	
	    }
	    
	    
	    
	    public boolean CalculateBill(int bill_id,int ord_id, int quantity,int del_con_id,int delivery_cost,int pub_id,int cost,int sum) throws EmployeeExceptionHandler{
	    	try
			{
	    		String updateTemp ="select * from bills "+"where bill_id = '"+bill_id+"'";; 
				stmt.executeQuery(updateTemp);
				System.err.println(updateTemp);	
				
			}
			catch (SQLException sqle)
			{
				System.err.println("Error with  calculate:\n"+sqle.toString());
			}
			
	    	if (sum==quantity*cost+delivery_cost){
	    		return true;
	    		
	    	}
	    	else return false;

			
		}
		
		//This is DBController test choose day of week code
		public boolean selectday(int id, String day) {
			try
			{
				String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
				rs=stmt.executeQuery(updateTemp);			
				if(rs.next()){
					System.out.println("Select Day Unsuccess!");
					return true;
				}
				else
				return false;

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with select:\n"+sqle.toString());
				return false;
			}
		}
		
		//This is DBController test choose Sunday of week code
		public boolean selectsunday(int id, String day) {
			try
			{
				String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
				rs=stmt.executeQuery(updateTemp);			
				if(rs.next()){
					System.out.println("Select Sunday Success!");
					return true;
				}
				else
				return false;

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with select:\n"+sqle.toString());
				return false;
			}
		}
		
		//This is DBController test choose Monday of week code
		public boolean selectmonday(int id, String day) {
			try
			{
				String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
				rs=stmt.executeQuery(updateTemp);			
				if(rs.next()){
					System.out.println("Select Monday Success!");
					return true;
				}
				else
				return false;

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with select:\n"+sqle.toString());
				return false;
			}
		}
		
		//This is DBController test choose Tuesday of week code
		public boolean selecttuesday(int id, String day) {
			try
			{
				String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
				rs=stmt.executeQuery(updateTemp);			
				if(rs.next()){
					System.out.println("Select Tuesday Success!");
					return true;
				}
				else
				return false;

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with select:\n"+sqle.toString());
				return false;
			}
		}
		
		//This is DBController test choose Wednesday of week code
		public boolean selectwednesday(int id, String day) {
			try
			{
				String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
				rs=stmt.executeQuery(updateTemp);			
				if(rs.next()){
					System.out.println("Select Wednesday Success!");
					return true;
				}
				else
				return false;

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with select:\n"+sqle.toString());
				return false;
			}
		}
		
		//This is DBController test choose Thursday of week code
		public boolean selectthursday(int id, String day) {
			try
			{
				String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
				rs=stmt.executeQuery(updateTemp);			
				if(rs.next()){
					System.out.println("Select Thursday Success!");
					return true;
				}
				else
				return false;

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with select:\n"+sqle.toString());
				return false;
			}
		}
		
		//This is DBController test choose Friday of week code
		public boolean selectfriday(int id, String day) {
			try
			{
				String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
				rs=stmt.executeQuery(updateTemp);			
				if(rs.next()){
					System.out.println("Select Friday Success!");
					return true;
				}
				else
				return false;

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with select:\n"+sqle.toString());
				return false;
			}
		}
		//This is DBController test choose Saturday of week code
		public boolean selectsaturday(int id, String day) {
			try
			{
				String updateTemp ="select * from daysOfWeek "+"where day_id= '"+id+"'"+"and dayOW= '"+day+"'";			
				rs=stmt.executeQuery(updateTemp);			
				if(rs.next()){
					System.out.println("Select Saturday Success!");
					return true;
				}
				else
				return false;

			}
			catch (SQLException sqle)
			{
				System.err.println("Error with select:\n"+sqle.toString());
				return false;
			}
		}
		//This is DBController test list of publication menu code
		public List<Publication> list1(){
			List<Publication> PublicationList = new ArrayList<Publication>();
		     try {
		        String selectPublication="SELECT * FROM publications";
		        rs= stmt.executeQuery(selectPublication);
		        while (rs.next()) {
		        	Publication p = new Publication(rs.getInt("pub_id"), rs.getString("pubName"), rs.getInt("cost"));
		        	PublicationList.add(p);
		        }
		     } catch (SQLException sqle) {
		    	 sqle.printStackTrace();
		     }
		     return PublicationList;
		  }
		
		
		public String checkdeliveryStatus(int orderID)throws SQLException {
			String s2 = "";
			try {
				String updateTemp = "select delivery_status from orders where ord_id="+orderID+"";
				rs = stmt.executeQuery(updateTemp);
				while(rs.next()){
				
				 s2 = rs.getString(1);
				
				
				}
				
				 
				
			} catch (SQLException sqle) {
				System.err.println("Error with login:\n" + sqle.toString());
				throw new SQLException ("failed to get status");
			}
			
			return s2;

		}

		public String checkPaymentStatus(int orderID)throws SQLException {
			String s2 = "";
			try {
				String updateTemp = "select payment_status from orders where ord_id="+orderID+"";
				rs = stmt.executeQuery(updateTemp);
				while(rs.next()){
					
					 s2 = rs.getString(1);
					
					
					}

			} catch (SQLException sqle) {
				System.err.println("Error with login:\n" + sqle.toString());
				throw new SQLException ("failed to get status");
			}
			return s2;

		}
		
		public boolean updateDeliveryStatus(int id,String payment_status,String delivery_Status)throws SQLException {
			
			
			try{
				String updateTemp1 = "select * from orders "+"where cust_id="+id+" and delivery_status='"+delivery_Status+"'";
				rs = stmt.executeQuery(updateTemp1);
				if(rs.next()){
					String updateTemp="UPDATE orders SET"+" delivery_status= '"+delivery_Status+"' where cust_id="+id+" and payment_status='"+payment_status+"'";
					stmt.executeUpdate(updateTemp);
					System.out.println("you successfully change delivery_status to "+delivery_Status);
					return true;
				}
				else{
					System.out.println("no value exists for this customer");
					throw new SQLException ("failed to update");
				}
				
				
			}
			catch(Exception sqle){
				System.err.println("Error with update:\n" + sqle.toString());
				throw new SQLException("failed to update");
			}
			

			
		}
		
	   public boolean updatepaymentStatus(int id,String payment_status,String delivery_Status)throws SQLException {
			
			
			try{
				String updateTemp2 =  "select * from orders "+"where cust_id="+id+" and payment_status='"+payment_status+"'";
				rs = stmt.executeQuery(updateTemp2);
				if(rs.next()){
					String updateTemp="UPDATE orders SET"+" payment_status= '"+payment_status+"' where cust_id="+id+" and delivery_status='"+delivery_Status+"'";
					stmt.executeUpdate(updateTemp);
					System.out.println("you successfully change payment_status to "+payment_status);
					return true;
				}
				else{
					System.out.println("no value exists for this customer");
					throw new SQLException ("failed to update");
				}
				
				
			}
			catch(Exception sqle){
				System.err.println("Error with update:\n" + sqle.toString());
				throw new SQLException ("failed to update");
			}
		}
	   public int changeDeliveryCost(float cost) {
			String query = "UPDATE Delivery_Config SET cost= '" + cost+ "; ";
	        try {
	            rs = stmt.executeQuery(query);
	        } catch (SQLException e) {
	            e.printStackTrace();
	        }
	        return 0;
		}

}
