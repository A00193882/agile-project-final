package daoClasses;

public class DbExceptionHandler extends Exception {
	
	String message;
	
	public DbExceptionHandler(String errMessage){
		message = errMessage;
	}
	
	public String getMessage() {
		return message;
	}

}

